# COV19-RD
Rewritten (python only) of https://github.com/allissondantas/BIOCOMSC-COVID19-MAT2PY

# BIOCOMSC-COVID19-MAT2PY
This is a translation Matlab to Python code for the Analysis and prediction of COVID-19 created by (BIOCOMSC) and avalaible in https://biocomsc.upc.edu/en/covid-19/situation-and-prediction .

### Project Description:
We employ an empirical model, verified with the evolution of the number of confirmed cases in previous countries where the epidemic is close to conclude, including all provinces of China. The model does not pretend to interpret the causes of the evolution of the cases but to permit the evaluation of the quality of control measures made in each state and a short-term prediction of tendencies. Note, however, that the effects of the measures’ control that start on a given day are not observed until approximately 5-7 days later.

Project authors: Martí Català, MD; Pere-Joan Cardona, PhD; Clara Prats, PhD; Sergio Alonso, PhD; Daniel López, PhD.
